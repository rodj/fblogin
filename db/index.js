require('dotenv').config();
const { Pool, QueryArrayResult } = require('pg')
const rtab = "\n            "
const connectionString = process.env.DATABASE_URL;

// TODO8Rodj use environment var or some other technique to ensure SSL is used on production
// const pool = new Pool(
//     {connectionString,
//       ssl: {
//             rejectUnauthorized: false
//           }}
// )
// For now, hardcode to false
const pool = new Pool({
    connectionString,
    ssl: false
})

/**
 * Asynchronous, sanitized Postgres query override.
 * @example
 * // Import db
 * const db = require('../db')
 *
 * // Create a result set
 * rs = db.query('select * from fbapps where isactive = $1', [true])
 *
 * // Accessing name of resultset
 * appname = rs.rows[0]['name']
 *
 * @param {string} sql SQL Statement with $1, $2 in place of parameters following postgress formatting
 * @param {any[]} params A list of parameters which will safely update the paramters used in the SQL statement. List number start a 1.
 * @returns {Promise<QueryArrayResult<R>>} Postgres result set objce
 */
async function query(sql, params) {
    const start = Date.now()
    const res = await pool.query(sql, params)
    const duration = Date.now() - start
    console.log(`\nExecuted query:${rtab}${sql}${rtab}\nParameters:${rtab}${params}\nDuration: ${duration}\nRow Count: ${res.rowCount}`)
    return res
}

async function log(action, details = '', appid = '', userid = '') { // regular try/catch does not work, postgres is complicated. async is irrelevant.
    appid = appid || ''
    userid = userid || ''

    pool.query(`INSERT INTO Log_ (Action, Details, AppId, UserId) VALUES ($1, $2, $3, $4)`, [action, details, appid, userid]).then(res => {
        // if there is something you want to do on success, it should be done here
    }).catch(e => {
        console.log('ERROR');
        console.log(e)
    })
}

module.exports = {
  query, log, QueryArrayResult
}
