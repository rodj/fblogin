
const db = require('../db')

/** @constant
    @type {Promise<db.QueryArrayResult<R>>}
    @example
    //applist is the resultset returned when the db promise resolves, you can name
    // however you choose, then refer to it in the callback function. This pattern is
    // required bc we don't know how long the db will take to respond
    helpme.queries.getActiveApps().then( (appList)=>{
        if(appList.rowCount==0){
        console.warn('No active apps found in db. Update fbapps table.')
        res.render('invites',null)
        return
        }
    // get the first name
    appname = appList.rows[0]['name]
    });
*/
async function getActiveApps(params) {
    return await db.query('select id,name from fbapps where isactive = true order by name, id',null)
}

async function getActiveUsers(proemail) {
    return await db.query('select id from testers where UPPER(proemail) = $1', [proemail.toUpperCase()])
}

async function isActiveUser(proemail) {
    const uppermail = proemail.toUpperCase()
    const sql = 'SELECT COUNT(*) FROM testers WHERE UPPER(proemail) = $1'
    const parms = [uppermail]
    const ret = false

    await db.query(sql, parms, (err, res) => {
        if (err) {
            console.log(err)
        } else {
            ret = res.rows[0] > 0
        }
    })

    return ret
}

module.exports = {
    getActiveApps, getActiveUsers, isActiveUser
}