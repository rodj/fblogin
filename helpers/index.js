const isValidEmail = require('./email')
const queries = require('./queries.js')
module.exports = {
    isValidEmail,
    queries
}