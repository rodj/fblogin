const Router = require('express-promise-router')
const db = require('../db')
const router = new Router()
var passport = require('passport');
const {setPassportStrategy,tokens,crypto} = require('../fbauth')
const helpme = require('../helpers')

/**
 * This file contains all necesary queries/actions for
 * interfacing with the the FBApp table. The FBApp
 * table holds app level information such as id, permissions
 * and other meta.
 *
 * All routes below are prefaced with /init
 *
 *  TODO:
 *    All route inputs require santization asap:
 *    - https://express-validator.github.io/docs/
 *    - https://flaviocopes.com/express-sanitize-input/
 */

/**
 * This route handles individual 'log in with facebook' functionality. We should provide more
 * details here such as:
 * TODO02:
 *  - Which app is the user logging into
 *  - What permissions will the user be prompted for
 *  - In a perfect world, this would be test step driven, with instructions for the user to complete at this
 *      specific step, for example 'Prior to clicking this link, make sure to revoke consent for <permisison>'
 *      or 'when prompted to consent, click the edit button <screenshot> and take a screenshot of all permissions'
 *
 */
router.get('/:proemail/:appId', async (req, res) => { //init/russell.lamb@protiviti.com/appid
  // console.log('loading email/app page')

  /**
   * The user must provide a valid app id, else the necessary info for app login
   * cannot not be generated. If this is a new app id, update fbapps table to
   * include the app meta data.
   *
   * TODO:
   *  - should be possible to auto generate fbapp entry using the developer api
   *
   */
  const proemail = req.params.proemail
  const appId = req.params.appId
  console.log(`ENTER: router.get /${proemail}/${appId}`)

  const app = await db.query('SELECT * FROM fbapps WHERE id = $1 and isActive=true',[appId])
  if (app.rowCount == 0){
    // SSTI Risk
    res.send(`Please request a valid registration link.`)
    msg = `/init/${proemail}/${appId} : No row in FBApps table for appid with isActive=true`
    console.warn(msg)
    db.log('Fail', msg)
    return
  }

  if (!helpme.isValidEmail(proemail)){
    // Do not tell the user what is wrong with the url, it can provide uninvited guests from inferring
    // controller logic and open ourselves to SSTI attacks
    res.send("Please request a valid registration link.")
    msg = `/init/${proemail}/${appId} : Invalid email`
    console.warn(msg)
    db.log('Fail', msg)
    return
  }

  ins_user = await db.query(`INSERT INTO testers
                (proemail)
                VALUES ($1)
                ON CONFLICT ON CONSTRAINT proemail
                DO UPDATE set proemail = $1
                RETURNING id`, [proemail.toLowerCase()]);

  setPassportStrategy(app.rows[0]['id'])
  req.session.proId = ins_user.rows[0]['id']

  /**
   * Data to be sent to client side for to build the manual facebook login button
   */
  res.render('fb_login',
    {
      userId: ins_user.rows[0]['id'],
      userperms: app.rows[0]['perms'],
      error: '',
      appId: appId,
      appName:app.rows[0]['appname']
    }
  )
})

/**
 * Default route, requires valid params to function
 */
router.get('/', async (req, res) => {
  res.send('Welcome. Please request a registration link')
})

module.exports = router