const db = require('../db')
const Router = require('express-promise-router')
const router = new Router()
const helpme = require('../helpers')

console.log("info route loaded")

router.get('/tos', async (req, res) => {
  res.render('tos')
})

router.get('/', async (req, res) => {
  const dtnow = new Date()
  console.log(`console.log for site root at ${dtnow}`)
  data = {'now': new Date(), 'jsval': '2023-03-14 14:40 Tue'}
  res.render('home', data)
})

router.get('/invites/:email', async (req, res) => {
  email = req.params.email

  helpme.queries.getActiveUsers(email).then((userlist) => {
    if (userlist.rowCount == 0) {
      data = {message: "Please request a valid invite link"}
      db.log('InvalidUser', email)
      res.render('invites', data)
      return
    }
  })

  /**
   *
   *
   */
  helpme.queries.getActiveApps().then((appList)=>{
    if(appList.rowCount==0){
      console.warn('No active apps found in db. Update fbapps table.')
      data = {message: "Please request a valid apps"}
      res.render('invites', data)
      return
    }

    data = {}
    invites = []
    console.log('Iterating over ',appList.rowCount, ' apps')
    for (let i = 0; i < appList.rowCount; i++) {
      appId = appList.rows[i]['id']
      appName = appList.rows[i]['name']
      if (process.env.APPURL == 'localhost') {
        url = `${process.env.PROTOCOL}://${process.env.APPURL}:${process.env.APPPORT}/init/${email}/${appId}`
      } else {
        url = `${process.env.PROTOCOL}://${process.env.APPURL}/init/${email}/${appId}`
      }
      invite = [appName,url]
      console.log(invite)
      invites.push(invite)
      data.invites = invites
    }
    console.log(data.invites)
    res.render('invites',data)
  })
})

router.get('/privacy', async (req, res) => {
    res.render('privacy_policy')
  })

module.exports = router;