// ./routes/index.js

// for each controller, import it and give it a name.
const api = require('./api')
const datadelete = require('./datadelete')
const deauthorize = require('./deauthorize')
const auth = require('./auth')
const fbapp = require('./fbapp')
const info = require('./info')
const pixel = require('./pixel')

// passing a server object here allows us to
// keep all our route assignments in one place.
// We could have manually set this app.js, but
// this is cleaner, albeit a bit obsfucated.

module.exports = server => {
    server.use('/', info)
    server.use('/datadelete', datadelete)
    server.use('/deauthorize', deauthorize)
    server.use('/api', api)
    server.use('/init', fbapp)
    server.use('/register/', auth)
    server.use('/pixel/', pixel)
}