const Router = require('express-promise-router')
const router = new Router()
const db = require('../db')

// This route is substantially parallel to route datadelete.
// If you make a change here, you probably need to change datadelete
console.log('Deauthorize route loaded')

// Is there any way to get the userid from Facebook?
router.get('/:appId', async (req, res) => {
    action = 'Deauthorize'
    console.log(`${action} appId=${req.params.appId}`)
    db.log(action, '', req.params.appId, 0)
    res.send(`${action} success`)
})

module.exports = router