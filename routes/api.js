const Router = require('express-promise-router')
const router = new Router()
const db = require('../db')

console.log("api route loaded")

/*

    Welcome to the PFTST API! This is used to integrate
    our tests results table with other testing workflows
    such as Oculus. You can make an API call from the headset, 
    instant game, or other testing channels and the results will
    be stored in line with other standard tests. From there we can 
    build VTs/XRs like normal. I've used a simple password check as 
    the primary means of security, but we should also implement an
    on off switch for disabling when not in use.
    
*/

router.post('/', async (req, res) => {
    data = JSON.parse(req.body.results)
    if (req.body.tomayto==process.env.TOMAHTO){
        console.log(data)
        dataSorted = [
            data['testid'],
            data['appid'],
            data['userid'],
            data['executedon'],
            data['executedonstr'],
            data['permission'],
            data['returncode'],
            data['code'],
            data['datareturned'],
            data['result']
        ]
        res.send(dataSorted)
    }
  })

module.exports = router