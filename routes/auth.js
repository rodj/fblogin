const Router = require('express-promise-router')
const router = new Router()
var passport = require('passport');
const request = require('request-promise');
const db = require('../db')
const {setPassportStrategy, tokens, crypto} = require('../fbauth')

console.log("auth route loaded")

/**
 *  Everything in this route should be limited to authentication
 *  this includes all pages AFTER the user clicks 'login with facebook'
 *  including callbacks from facebook
 */

/**
 *
 * @returns
 */

function isProtivitiUser() {
    // console.log(req.session.proId)
    return (req.session.proId == undefined || req.session.proId == '')
}

// This handles what happens when user clicks on our 'Log in with Facebook' link
router.get('/:appId', //expects /register/appId?perms=email,public_profile
  function(req, res, next) {
    //   console.log('Register/app id page')
    var appId = req.params.appId;
    if(!isProtivitiUser) {
        e = 'An error occured in locating your user id. Start from the original email link you recieved or contact Russ Lamb.'
        console.log(e)
        res.send(e);
        return
    }
    var perms = req.query.perms.split(',');
    if(req.session) {
        req.session.perms = perms;
    } else {
        e = "Missing session data..."
        console.log(e)
        send(e)
        return
    }

    // Track whenever user has been sent to the FBLogin page
    db.log('FBLoginRequested', '', appId, req.session.proId)

    setPassportStrategy(appId)
    passport.authenticate(appId, {
      scope: perms
    })(req,res,next);
  }
);

router.get('/return/:appId',
    function(req, res, next) {
      var appId = req.params.appId;

      console.log(`Handling route /return/${appId}`);

      setPassportStrategy(appId);

      passport.authenticate(appId, {
        failureRedirect: '/error',
        successRedirect: '/register/complete/'+appId,
        failureFlash: 'Something broke. Unable to authenticate.'
      })(req, res, next);
    }
);

router.get('/complete/:appId',
  require('connect-ensure-login').ensureLoggedIn(),
  function(req, res) {
    var appId = req.params.appId;
    console.log(`Request the long-lived token for appId=${appId}`);

    const options = {
      method: 'GET',
      uri: `https://graph.facebook.com/v15.0/oauth/access_token`,
      qs: {
        grant_type: 'fb_exchange_token',
        client_id: appId,
        client_secret: tokens[appId],
        fb_exchange_token: req.user.token
      }
    };

     request(options)
    .then(async (fbRes) => {
      const parsedRes = JSON.parse(fbRes);
      hashedToken = crypto(parsedRes['access_token']);

      ins_data = [
          appId                // appid
          ,req.session.proId   // userid
          ,req.user.id         // asid
          ,hashedToken         // token
          ,req.session.perms   // perms
      ]

      console.log("Storing hashed token")
      console.log(ins_data)

      //store userId and hashed token with secret.
      insert = await db.query(`
                      INSERT INTO tokens(
                          appid, userid, asid, token, perms, updated)
                      VALUES ($1, $2, $3, $4, $5, now())
                      ON CONFLICT (asid) DO
                      UPDATE SET
                          token = $4, updated= now(), perms =$5
                      RETURNING *;`,
                      ins_data);
      // console.log(insert);
      // store_token(req.user.id, appId, req.session.userId, req.session.perms, hashedToken);
      // res.render('complete', {
      //    results: hashedToken,
      //    appname: apps[appId][0]
      //  });

      // Track whenever user has been sent to the FBLogin page
      db.log('FBLoginComplete', '', appId, req.session.proId)

      // TODO5 Handle the Facebook offered callback for registration

      let d = new Date();
      res.send(`<p>App registration success at ${d.toISOString()}</p><p>AppId=${appId}, UserId=${req.session.proId}.</p>`)
    });
  }
);

module.exports = router