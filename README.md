
# FB Login Server

Login server for FB
## Authors
- [@ruslam01](https://github.com/ruslam01)
- [@rodjpro](https://github.com/rodjpro)




## Required Software

- node
- npm
- Postgres
- TODO5 Data Dictionary

## Features

- Nothing run locally - see fb0.5 for that

- Has its own repo with a hook. Commits to it will trigger a build and deploy.

- Generally want to work at top level.

Uses:
- node.js
- express.js
- passport (Oauth)
    - Passport mainly handles auth, but it has been customized to auto-generate separate login per app. App link per user per app Uses Passport Module/plugin for authentication/session cache