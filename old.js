// url uses username as lookup
// gets permissions and app list 
require('dotenv').config();
const request = require('request-promise');

// Web server setup
var express = require('express');
var passport = require('passport');
var Strategy = require('passport-facebook').Strategy;
var app = express();

// Configure view engine to render EJS templates.
app.enable("trust proxy");
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

// Use application-level middleware for common functionality, including
// logging, parsing, and session handling.
app.use(require('morgan')('combined'));
app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({ extended: true }));
app.use(require('express-session')({ secret: 'keyboard cat', resave: true, saveUninitialized: true }));

app.get('/',
function(req, res) {
    var userId = req.params.userId;
    res.render('<h1>Hello</h1>');
});
app.get('/:userId',
function(req, res) {
    var userId = req.params.userId;
    res.render('app_list', {
        userId:userId
    })
});

app.listen(8080);
console.log("Loaded!");