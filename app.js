/*
    Welcome to FBLoginServer Application Entry Point! This file handles
    the entire website and sets up the major plugins including logging,
    sessions management, routes, and just about anything globally necessary
    across pages.

    Execution:
      To run the web server, execute the following from the root directory
      > node app.js
      Additionally, you use a watcher plugin such as nodemon, but make sure you exclude any log directories using -i (ignore)
      > nodemon -i /logs app js
      The package.json file dictates that app.js is the entry point, so these also works
      > npm start
      > nodemon -i /logs
      > nodemon

    How to: Add a new route
        TODO3 instructions here? Wiki?
*/

// Web server plugin
const express = require('express');
const server = express();

// Authentication handler
const passport = require('passport');

// used for http log formating
var morgan = require('morgan')
server.use(morgan('tiny'));

server.use(require('cookie-parser')());
server.use(require('body-parser').urlencoded({ extended: true }));
server.use(require('express-session')({
   secret: 'when in doubt roll it back',
   resave: true,
   saveUninitialized: true
}));
server.use(passport.initialize())
server.use(passport.session())

// enable cors from facebook
const cors = require('cors');
server.use(cors({
   origin: '*'
}));

passport.serializeUser(function(user, cb) {
   cb(null, user);
})

passport.deserializeUser(function(obj, cb) {
   cb(null, obj);
})

// Configure Routes
//    Set the routes directory, which resolves automagically to index.js in the routes folder
const mountRoutes = require('./routes');

//    Set the views directory
server.set('views', __dirname + '/views');
//    Configure the view engine,
//      this tells node what type of mvc (model view controller) view
//      framework to use. Becareful not to introduce SSTI's here by making
//      sure we never allow for user input to modify the page
//          https://securityintelligence.com/posts/how-to-protect-server-side-template-injection/
server.set('view engine', 'ejs');
//
mountRoutes(server);

// PORT should only exist on the prod box
port = process.env.PORT || process.env.APPPORT;

server.listen(port);

const msg = `Loaded server. Accepting connections at localhost:${port}`
console.log(msg)