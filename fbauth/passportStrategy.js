var passport = require('passport');
var Strategy = require('passport-facebook').Strategy;
const tokens = require('./tokens')

function setPassportStrategy(appId) {
    console.log(`ENTER setPassportStrategy appId=${appId}; clientSecret=tokens[appId]=${tokens[appId]}`);

    if (process.env.APPURL == 'localhost'){
        callback = `${process.env.PROTOCOL}://${process.env.APPURL}:${process.env.APPPORT}/register/return/${appId}`
    } else {
        callback = `${process.env.PROTOCOL}://${process.env.APPURL}/register/return/${appId}`
    }
    passport.use(appId, new Strategy({
        clientID: appId,
        clientSecret: tokens[appId],
        profileFields: ['id', 'email', 'name'],
        callbackURL: callback,
        enableProof: true
        },
        function(accessToken, refreshToken, profile, cb) {
            // console.log('loaded strategty for app id: %s', appId)
            // In this example, the user's Facebook profile is supplied as the user
            // record.  In a production-quality application, the Facebook profile should
            // be associated with a user record in the application's database, which
            // allows for account linking and authentication with other identity
            // providers.
            profile.token = accessToken;
            //console.log(profile);
            // session.token = accessToken;
            return cb(null, profile);
            }
        )
    );


}

module.exports = (appId) => setPassportStrategy(appId)