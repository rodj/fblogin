require('dotenv').config();
const crypto = require('crypto'),
    resizedIV = Buffer.allocUnsafe(16),
    iv = crypto
      .createHash("sha256")
      .update("myHashedIV")
      .digest();
iv.copy(resizedIV);
const k = process.env.K
const mode = process.env.MODE
const algo = process.env.ALGO

function enc_str(str){
    const key = crypto
      .createHash(algo)
      .update(k)
      .digest(),
    cipher = crypto.createCipheriv(mode, key, resizedIV),
    msg = [];
    msg.push(cipher.update(str, "binary", "hex"));
    msg.push(cipher.final("hex"));
    hash = msg.join("");
    // console.log(hash);
    return hash;
  }

module.exports = str => enc_str(str)