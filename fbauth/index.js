const setPassportStrategy = require('./passportStrategy')
const tokens = require('./tokens')
const crypto = require('./crypto')

module.exports = {
    setPassportStrategy: (appId) => setPassportStrategy(appId),
    tokens: tokens,
    crypto: (str)=>crypto(str)
}