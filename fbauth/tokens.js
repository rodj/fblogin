var tokenList = process.env.FBS.split(',')
var tokens = {}

for (let i = 0; i < tokenList.length; i++) {
    var thisToken = tokenList[i].split(":")
    tokens[thisToken[0]]=thisToken[1]
}

module.exports = tokens